<?php

namespace App\Http\Controllers;

use App\EmployeeWorkHistory;
use Illuminate\Http\Request;

class EmployeeWorkHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeWorkHistory  $employeeWorkHistory
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeWorkHistory $employeeWorkHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeWorkHistory  $employeeWorkHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeWorkHistory $employeeWorkHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeWorkHistory  $employeeWorkHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeWorkHistory $employeeWorkHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeWorkHistory  $employeeWorkHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeWorkHistory $employeeWorkHistory)
    {
        //
    }
}
