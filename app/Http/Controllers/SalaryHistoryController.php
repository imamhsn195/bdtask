<?php

namespace App\Http\Controllers;

use App\Employee;
use App\SalaryHistory;
use App\SalarySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalaryHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'month' => 'required',
            'salary_employee_id' => 'required',
            'hours_worked_per_day' => 'required',
            'extraHolyDays' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $salary_setting = SalarySetting::whereActive('1')->latest()->first();
            if(!$salary_setting) {
                return redirect('/salary_settings')->withErrors('Please save salary Settings first');
            }else{
                $emp_id = $request->salary_employee_id;
                $employee = Employee::find($emp_id);
                $total_employees = Employee::count();

                $total_days = 30;
                $total_frydayes = 4;
                $total_working_hours = ($total_days - $total_frydayes)*$salary_setting->working_hours_per_day;
                
                $extra_holy_days = $request->extraHolyDays;
                $worked_per_day = $request->hours_worked_per_day;
                $total_worked_hours = ($total_days - $total_frydayes - $extra_holy_days) * $worked_per_day;

                $gross_salary = ($employee->basic * $total_worked_hours)/$total_working_hours;

                $provident_fund_deduction = $employee->basic * ($salary_setting->provident_fund_deduction_rate/100);
                
                $insurance_health_cost_deduction = ($salary_setting->yearly_insurance_health_cost * ( $salary_setting->insurance_health_cost_deduction_rate/100)/12);
                
                $revenue_share = 0;
                if($request->is_financial_year_ended){
                    $revenue_share = (($salary_setting->yearly_revenue * ($salary_setting->revenue_share_rate/100))/$total_employees);
                }

                $net_salary = $gross_salary - $provident_fund_deduction - $insurance_health_cost_deduction + $revenue_share;

                SalaryHistory::updateOrCreate([
                    'month' => $request->month,
                    'employee_id' => $emp_id
                ],[
                    'total_salary' => $net_salary,
                    'month' => $request->month,
                    'employee_id' => $emp_id
                ]);

                DB::commit();
                return redirect()->route('employees.show',$emp_id );
            }
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalaryHistory  $salaryHistory
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryHistory $salaryHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalaryHistory  $salaryHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryHistory $salaryHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalaryHistory  $salaryHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryHistory $salaryHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalaryHistory  $salaryHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryHistory $salaryHistory)
    {
        //
    }
}
