<?php

namespace App\Http\Controllers;

use App\SalarySetting;
use Illuminate\Http\Request;

class SalarySettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $salary_setting = SalarySetting::whereActive('1')->latest()->first();
        return view('salaries.index', compact('salary_setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ddd($request->all());
        SalarySetting::create($this->validate($request,[
            "provident_fund_deduction_rate" => "required",
            "yearly_insurance_health_cost" => "required",
            "insurance_health_cost_deduction_rate" => "required",
            "yearly_revenue" => "required",
            "revenue_share_rate" => "required",
            "working_hours_per_day" => "required"
        ]));
        session()->flash('status', 'Salary Settings Updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalarySetting  $salarySetting
     * @return \Illuminate\Http\Response
     */
    public function show(SalarySetting $salarySetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalarySetting  $salarySetting
     * @return \Illuminate\Http\Response
     */
    public function edit(SalarySetting $salarySetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalarySetting  $salarySetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalarySetting $salarySetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalarySetting  $salarySetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalarySetting $salarySetting)
    {
        //
    }
}
