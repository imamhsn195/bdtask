<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryHistory extends Model
{
    protected $fillable = ['month','employee_id','is_paid','total_salary'];

    
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    
}
