<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalarySetting extends Model
{
    protected $fillable = [
        'working_hours_per_day', 
        'provident_fund_deduction_rate',
        'yearly_insurance_health_cost',
        'insurance_health_cost_deduction_rate',
        'yearly_revenue',
        'revenue_share_rate'
    ];
}
