<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('working_hours_per_day');
            $table->integer('provident_fund_deduction_rate');
            $table->double('yearly_insurance_health_cost');
            $table->integer('insurance_health_cost_deduction_rate');
            $table->double('yearly_revenue');
            $table->integer('revenue_share_rate');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_settings');
    }
}
