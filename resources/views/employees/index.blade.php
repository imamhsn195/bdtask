@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee List
                    <a class="btn btn-link" href="/employees/create">Add New</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                    @endif
                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Due Salary</th>
                                <th>actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($employees as $employee)
                                <tr>
                                    <td>{{ $employee->id }}</td>
                                    <td><a href="{{route('employees.show', $employee->id)}}">{{$employee->name }}</a></td>
                                    <td>{{ $employee->address }}</td>
                                    <td>(Pending...)</td>
                                    <td>
                                        <button type="button" class="btn btn-primary generate_monthly_salary_id" data-toggle="modal" data-employee-id="{{ $employee->id }}" data-target="#exampleModal" id="generate_monthly_salary_id">
                                            Generate Monthly Salary
                                        </button>
                                    </td>
                                </tr>                                
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center bg-danger text-white">No record found</td>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="{{ route('salary_histories.store') }}" method="POST">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Generate Salary</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="salary_employee_id" id="salary_employee_id">
            <div class="form-group">
                <label for="monthly_salary">Month</label>
                <select name="month" id="month" class="form-control" required>
                    <option value="">Select Month</option>
                    <option value="0">January</option>
                    <option value="1">February</option>
                    <option value="2">March</option>
                    <option value="3">April</option>
                    <option value="4">May</option>
                    <option value="5">June</option>
                    <option value="6">July</option>
                    <option value="7">August</option>
                    <option value="8">September</option>
                    <option value="9">October</option>
                    <option value="10">November</option>
                    <option value="11">December</option>
                </select>
            </div> 

            <div class="form-group">
                <label for="hoursWorkedPerDayHelp">Hours Worked Per days</label>
                <input type="number" class="form-control" id="hours_worked_per_day" aria-describedby="hoursWorkedPerDayHelp" placeholder="e.g. 5" name="hours_worked_per_day" required>
                <small id="hoursWorkedPerDayHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="extraHolyDaysHelp">Total Extra Holy Days</label>
                <input type="number" class="form-control" id="extraHolyDays" name="extraHolyDays" aria-describedby="extraHolyDaysHelp" placeholder="e.g. 5" name="extra_holy_days" required>
                <small id="extraHolyDaysHelp" class="form-text text-muted"></small>
            </div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="is_financial_year_ended" name="is_financial_year_ended">
                <label class="custom-control-label" for="is_financial_year_ended">Is Financial Year Ended</label>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
    </div>
  </div>
</div>
    
@endsection
@section('js')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $('.generate_monthly_salary_id').on('click',function(){
            $('#salary_employee_id').val($(this).attr("data-employee-id"));
        });
    </script>
@endsection