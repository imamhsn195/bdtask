@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Month</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $salaty_history_sl = 1; @endphp
                            @forelse ($employee->salaty_histories as $salaty_history)
                                <tr>
                                    <td>{{ $salaty_history_sl++}}</td>
                                    <td>{{month($salaty_history->month) }}</td>
                                    <td> BDT: {{ number_format($salaty_history->total_salary,2) }}</td>
                                    <td>
                                        <span class="text-white p-2 badge badge-lg bg-{{ $salaty_history->is_paid ? 'success' : 'danger'}}">{{ $salaty_history->is_paid ? 'Paid' : 'Due'}}</span>
                                    </td>
                                    <td>
                                        {{-- <button type="button" class="btn btn-primary generate_monthly_salary_id" data-toggle="modal" data-salaty_history-id="{{ $employee->id }}" data-target="#exampleModal" id="generate_monthly_salary_id">
                                            Generate Monthly Salary
                                        </button> --}}
                                    </td>
                                </tr>                                
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center bg-danger text-white">No record found</td>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
