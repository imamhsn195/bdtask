@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Salary Setting</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('salary_settings.store')}}" method="POST">
                      @csrf
                        <div class="form-group">
                          <label for="Working_hour_per_day">Working Hour Per day</label>
                          
                          <input type="number" class="form-control" id="Working_hour_per_day" name="working_hours_per_day" aria-describedby="Working_hour_per_dayHelp" placeholder="0" value="{{$salary_setting->working_hours_per_day ?? ''}}">

                          <small id="Working_hour_per_dayHelp" class="form-text text-muted">{{"Employees' Working hours Per day"}}</small>
                        </div>                        
                        <div class="form-group">
                          <label for="number">Provident Fund Deduction( % of Basic)</label>
                          <input type="number" class="form-control" id="provident_fund_deduction_rate" name="provident_fund_deduction_rate" aria-describedby="Help" placeholder="e.g. 3 " value="{{$salary_setting->provident_fund_deduction_rate ?? ''}}">
                          <small id="provident_fund_deduction_rateHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="yearly_insurance_health_cost">Yearly Employee Insurance and health cost</label>
                          <input type="number" class="form-control" id="yearly_insurance_health_cost" name="yearly_insurance_health_cost" aria-describedby="yearly_insurance_costHelp" placeholder="0.00" value="{{$salary_setting->yearly_insurance_health_cost ?? ''}}">
                          <small id="yearly_insurance_costHelp" class="form-text text-muted">{{"Yearly Employees' Insurance cost"}}</small>
                        </div>
                        <div class="form-group">
                          <label for="number">Insurance and health cost Deduction( % of Total Insurance and health cost)</label>
                          <input type="number" class="form-control" id="insurance_health_cost_deduction_rate"  name="insurance_health_cost_deduction_rate" aria-describedby="Help" placeholder="e.g. 3 " value="{{$salary_setting->insurance_health_cost_deduction_rate ?? ''}}">
                          <small id="insurance_health_cost_deduction_rateHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="yearly_revenue">Yearly Revenue</label>
                          <input type="number" class="form-control" id="yearly_revenue" name="yearly_revenue" aria-describedby="yearly_revenueHelp" placeholder="0.00" value="{{$salary_setting->yearly_revenue ?? ''}}">
                          <small id="yearly_revenueHelp" class="form-text text-muted">{{"Yearly Employees' Insurance cost"}}</small>
                        </div>
                        <div class="form-group">
                          <label for="revenue_share_rateHelp">Revenue Share( % of Total Revenue) </label>
                          <input type="number" class="form-control" id="revenue_share_rate" name="revenue_share_rate" aria-describedby="revenue_share_rateHelp" placeholder="e.g. 5"  value="{{$salary_setting->revenue_share_rate ?? ''}}">
                          <small id="revenue_share_rateHelp" class="form-text text-muted"></small>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
